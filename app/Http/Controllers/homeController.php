<?php

namespace App\Http\Controllers;



use App\post;
use Illuminate\Http\Request;

class homeController extends Controller
{
    public function postSave(Request $request)
    {
        $post_title=$request->input('post_title');
        $post_description= $request->input("post_description");
        $post_object=new post();
        $post_object->post_title=$post_title;
        $post_object->post_description=$post_description;
        $post_object->save();
        return view("Home");
    }
    public  function post_view()
    {

        return view("Home");
    }


}
