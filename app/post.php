<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $fillable=['post_title','post_description'];
  public function student()
  {
    return  $this->belongsTo(student::class);
  }

}
