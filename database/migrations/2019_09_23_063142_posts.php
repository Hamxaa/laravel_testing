<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("posts", function (Blueprint $table){
            $table->bigIncrements("id")->unsigned();
            $table->unsignedBigInteger("student_id");
            $table->string("post_title","30");
            $table->string("post_description","100");
            $table->timestamp("updated_at")->nullable();
            $table->timestamp("created_at")->nullable();
            $table->foreign("student_id")->references("id")->on("students")->onDelete("cascade");
        });

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
