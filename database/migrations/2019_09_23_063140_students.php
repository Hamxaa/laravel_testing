<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Students extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("students",function (Blueprint $table){
           $table->bigIncrements("id");
           $table->string("name","30");
           $table->string("lastName","30");
           $table->string("email","30")->unique();
           $table->string("rollNumber","30")->unique();
            $table->timestamp("updated_at")->nullable();
            $table->timestamp("created_at")->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('students');
    }
}
