<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('posts',"homeController@post_view");
Route::get('Home/{id?}','homeController@show');
Route::post("post_submit","homeController@postSave")->name("post_save");
//Route::get('posts','homeController@posts');

